USE [NexGen_FCC_Software]
GO


/****** Object:  StoredProcedure [dbo].[FMS_SP_Sync_SF_NexGenWO]    Script Date: 6/24/2018 11:08:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- exec [FMS_SP_Sync_SF_NexGenWO] 
 
CREATE PROC [dbo].[FMS_SP_Sync_SF_NexGenWO]
AS
BEGIN

	Declare @ID nvarchar(18), @CaseNumber nvarchar(30), @tmpCaseNumber nvarchar(30), @Template nvarchar(50), @OwnerID varchar(50), @Activity nvarchar(50),
				@CreatedDate datetime, @Line int, @LC int, @LCC int, @WoEmp int, @SFCaseOwner nvarchar(1300), @ExtractDateTime datetime, @interror int, @WoIn int
				
	DECLARE SalesForce CURSOR fast_forward read_only FOR 
				-- Getting rows from both Main case table and Comments table based on the system mod date and identitying which line it is.
				SELECT ID, CaseNumber, Template, SFCaseOwner, CreatedDate, Activity, Line='1' FROM FMS_SF_DAILY_EXTRACT DE where SystemModStamp >= (SELECT LastSyncDate FROM FMS_SYNC WHERE Tablename = 'WORKORDER_SF')
				union all
				SELECT  ID, CaseNumber, Template, SFCaseOwner, CreatedDate, Activity, Line='2' FROM FMS_SF_DAILY_EXTRACT DE where ID in (SELECT ParentID FROM FMS_SF_CaseComments where SystemModStamp >= (SELECT LastSyncDate FROM FMS_SYNC WHERE Tablename = 'WORKORDER_SF'))
				ORDER BY CaseNumber, Line

		OPEN SalesForce
		FETCH NEXT FROM SalesForce INTO @ID, @CaseNumber, @Template, @SFCaseOwner, @CreatedDate, @Activity, @Line
		set @tmpCaseNumber = ''
		PRINT 'Before Inserting WO...'
			WHILE @@FETCH_STATUS = 0
			BEGIN
					-- Since we are getting based on the order of the Work order and line
					-- IF Line = 1 we check  Insert or Update all the 
					-- Master, detail, Action list, Employee and Inhouse.
					-- If the line = 2 then only we check for the  In house comments no need to touch other Master or Details.
					
					IF @tmpCaseNumber = @CaseNumber
					begin
							set @tmpCaseNumber = @CaseNumber
							GOTO NextRecord
					end
				IF NOT EXISTS (SELECT WoCode from FMS_WORKORDER_MASTER where WoCode = convert(varchar(15),@CaseNumber))
				BEGIN
				    PRINT 'Inserting WO...'
					--Inserting WORK ORDER MASTER
					 INSERT INTO dbo.FMS_WORKORDER_MASTER
						(
							WoCode, 
							CompanyCode,
							UnitOwner,
							UnitCode,
							DateIssued,
							WorkStatus,
							YardCode,
							DeptCode,
							PriorityID,
							WoReasonID,
							GroupNumb,
							WorkDesc,
							CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
						   )
					   SELECT 
						   de.CaseNumber
						   ,(SELECT top 1 CompanyCode FROM dbo.FMS_COMPANIES fc WHERE Name='Fleet Cost & Care, Inc.') AS CompanyCode
						   ,'N' as UnitOwner
						   , (SELECT Unitcode FROM FMS_UNITS WHERE UnitName=de.SFAccount) AS UnitCode 
						   ,de.CreatedDate AS DateIssued
						   , case when de.Status = 'Closed' and de.SystemModStamp < Getdate()-59 then 'C' when de.Status like 'Closed%' then 'F' else 'O' end   AS WorkStatus -- Closed cases in SF?
						   ,'SW' AS Yard
						   ,(select DeptCode from FMS_Departments where Name=de.Department AND Status=1) AS DeptCode
						   ,(SELECT WoPriorityID FROM dbo.FMS_WORKORDER_PRIORITY fwp where Description=de.Priority and status=1) AS PriorityID
						   ,(SELECT WOReasonID FROM dbo.FMS_WORKORDER_REASON fwr WHERE Reason=de.Type and status=1) as WOReasonID
						   ,'00' AS GroupNumb
						   ,de.Subject + char(13) + char(10) + de.description AS WODesc
						   ,'sa' AS CreatedBy, getdate() AS CreatedDate, 'sa' AS ModifiedBy, getdate() AS ModifiedDate
					   FROM FMS_SF_DAILY_EXTRACT de 
					   WHERE de.CaseNumber=@CaseNumber					  						
					    
					 -- This for Activity tab  where no predefined templates exists.
					 if @Template = ''
					 begin
							SELECT @LC = LastCounter FROM FMS_SEQUENCE_CONTROL WHERE TableName = 'wo_detail'
							set @LC = @LC+1
							update FMS_SEQUENCE_CONTROL set LastCounter = @LC where TableName = 'wo_detail'
							
							INSERT INTO FMS_WORKORDER_DETAIL 
							(
								WoDetailID,
								WoCode, 
								ActivityCode, 
								Comments,
								CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
							) 
							SELECT @LC,
								de.CaseNumber,
								de.Activity,
								de.Subject,
								'sa' AS CreatedBy, getdate() AS CreatedDate, 'sa' AS ModifiedBy, getdate() AS ModifiedDate
							FROM FMS_SF_DAILY_EXTRACT DE
							WHERE DE.CaseNumber = @CaseNumber
							
							SELECT @LCC = LastCounter FROM FMS_SEQUENCE_CONTROL WHERE TableName = 'WO_ACT_ACTION_LIST'
							set @LCC = @LCC+1
							update FMS_SEQUENCE_CONTROL set LastCounter = @LCC where TableName = 'WO_ACT_ACTION_LIST'
							
							INSERT INTO FMS_WO_ACTION_LIST
							(
								WoActionListID,
								WoCode,
								ActivityCode,
								ActionList,
								CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
							)
							select @LCC,
									de.CaseNumber, 
									de.Activity,
									de.Subject,
									'sa' AS CreatedBy, getdate() AS CreatedDate, 'sa' AS ModifiedBy, getdate() AS ModifiedDate
							FROM FMS_SF_DAILY_EXTRACT DE 
							WHERE DE.CaseNumber = @CaseNumber
					   end
					   ELSE -- This for TemplateActivity tab where Template is not null , need to get the pre defined activities
					   BEGIN
							 
							SELECT @LC = LastCounter FROM FMS_SEQUENCE_CONTROL WHERE TableName = 'wo_detail'
							set @LC = @LC+1
														
							INSERT INTO FMS_WORKORDER_DETAIL 
							(
								WoDetailID,
								WoCode, 
								ActivityCode, 
								Comments,
								CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
							) 
							SELECT @lc + ROW_NUMBER() Over (Order by WoDetailID),
								@CaseNumber, 
								Activitycode,
								Comments,
								'sa' AS CreatedBy, getdate() AS CreatedDate, 'sa' AS ModifiedBy, getdate() AS ModifiedDate
							FROM FMS_WORKORDER_DETAIL FWD 
							WHERE FWD.WoCode=@Template 
							
							update FMS_SEQUENCE_CONTROL set LastCounter = (select MAX(WoDetailID) FROM FMS_WORKORDER_DETAIL) where TableName = 'wo_detail'
							
							SELECT @LCC = LastCounter FROM FMS_SEQUENCE_CONTROL WHERE TableName = 'WO_ACT_ACTION_LIST'
							set @LCC = @LCC+1
							--update FMS_SEQUENCE_CONTROL set LastCounter = @LCC where TableName = 'WO_ACT_ACTION_LIST'
							
							INSERT INTO FMS_WO_ACTION_LIST
							(
								WoActionListID,
								WoCode,
								ActivityCode,
								ActionList,
								CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
							)
							select  @lcc + ROW_NUMBER() Over (Order by WoActionListID),
									@CaseNumber, 
									Activitycode,
									ActionList,
									'sa' AS CreatedBy, getdate() AS CreatedDate, 'sa' AS ModifiedBy, getdate() AS ModifiedDate
							FROM FMS_WO_ACTION_LIST FWO 
							WHERE FWO.WoCode = @Template
							
							UPDATE FMS_SEQUENCE_CONTROL set LastCounter = (select MAX(WoActionListID) FROM FMS_WO_ACTION_LIST)
										WHERE TableName = 'WO_ACT_ACTION_LIST'
						END
						
							-- Adding Employee in Employee tab
							SELECT @WoEmp = LastCounter FROM FMS_SEQUENCE_CONTROL WHERE TableName = 'wo_emp'
							set @WoEmp = @WoEmp+1
							update FMS_SEQUENCE_CONTROL set LastCounter = @WoEmp where TableName = 'wo_emp'
							 
							 IF NOT EXISTS (SELECT * FROM FMS_EMPLOYEES FE WHERE FE.FName + ' ' + FE.LName = @SFCaseOwner)
									SET @SFCaseOwner = 'Bill Hollowsky'
							 
							INSERT INTO FMS_WORKORDER_EMPLOYEE
							(
								WoEmpID, WoCode, ActivityCode, EmpTypeID, EmpID, GroupNumb, WorkDate, EndDate,
								CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
							)
							SELECT TOP 1 @WoEmp, @CaseNumber, @Activity, FE.EmpTypeID, FE.EmpID, FE.GroupNumb,
							@CreatedDate, @CreatedDate,
							'sa' AS CreatedBy, getdate() AS CreatedDate, 'sa' AS ModifiedBy, getdate() AS ModifiedDate
							FROM FMS_EMPLOYEES FE
							WHERE FName + ' ' + LName = @SFCaseOwner
							ORDER BY EMPID DESC
							
							--Adding IN House Comments
							SELECT @WoIn = LastCounter FROM FMS_SEQUENCE_CONTROL WHERE TableName = 'WO_INHOUSE_COMMENTS'
							set @WoIn = @WoIn+1
							
							INSERT INTO FMS_WO_INHOUSE_COMMENTS
							(
								WOInHouseID, WoCode, Comments, AddedFrom,
								CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
							)
							SELECT @WoIn + ROW_NUMBER() Over (Order by ID), @CaseNumber, cc.CommentBody, 'W', 
							case when (Select top 1 de.SFCaseOwner from FMS_SF_DAILY_EXTRACT de where de.OwnerID = cc.CreatedByID) like 'Product Dev Team%' then 'Sagar Maramreddy' 
									when (SELECT EmpID FROM FMS_EMPLOYEES FE WHERE FE.FName + ' ' + FE.LName = (Select top 1 de.SFCaseOwner from FMS_SF_DAILY_EXTRACT de where de.OwnerID = cc.CreatedByID)) is null then 'Bill Hollowsky' 
									else  (Select top 1 de.SFCaseOwner from FMS_SF_DAILY_EXTRACT de where de.OwnerID = cc.CreatedByID)
								END,
							cc.CreatedDate, 
							case when (Select top 1 SFCaseOwner from FMS_SF_DAILY_EXTRACT de where de.OwnerID = cc.LastModifiedByID) like 'Product Dev Team%' then 'Sagar Maramreddy' 
									when (SELECT EmpID FROM FMS_EMPLOYEES FE WHERE FE.FName + ' ' + FE.LName = (Select top 1 de.SFCaseOwner from FMS_SF_DAILY_EXTRACT de where de.OwnerID = cc.LastModifiedByID)) is null then 'Bill Hollowsky' 
									else (Select top 1 de.SFCaseOwner from FMS_SF_DAILY_EXTRACT de where de.OwnerID = cc.LastModifiedByID)
									END,
							cc.LastModifiedDate
							FROM FMS_SF_CaseComments cc WHERE ParentId = @ID
							 
							UPDATE FMS_SEQUENCE_CONTROL set LastCounter = (select MAX(WOInHouseID) FROM FMS_WO_INHOUSE_COMMENTS)
										WHERE TableName = 'WO_INHOUSE_COMMENTS'
							
					INSERT INTO dbo.FMS_WO_SF_Sync_Log
					(
					    ID,
					    CaseNumber,
					    SyncAction,
					    CreatedDate
					)
					VALUES (@ID, @CaseNumber, 'I', GETDATE())
				END					  
				ELSE -- Update NexGen WO

				BEGIN

						PRINT 'Updating WO...' + @CaseNumber 
						 
					IF @Line = '1'
					BEGIN
						UPDATE FMS_WORKORDER_MASTER 
						SET
							CompanyCode = (SELECT top 1 CompanyCode FROM dbo.FMS_COMPANIES fc WHERE Name='Fleet Cost & Care, Inc.'),
							UnitOwner = 'N',
							UnitCode = (SELECT UnitCode FROM FMS_UNITS WHERE UnitName=de.SFAccount),
							DateIssued = DE.CreatedDate,
							WorkStatus = case when de.Status = 'Closed' and de.SystemModStamp < Getdate()-59 then 'C' when de.Status like 'Closed%' then 'F' else 'O' end,
							YardCode = 'SW',
							DeptCode = (select DeptCode from FMS_Departments where Name=de.Department AND Status=1),
							PriorityID = (SELECT WoPriorityID FROM dbo.FMS_WORKORDER_PRIORITY fwp where Description=de.Priority and status=1),
							WoReasonID = (SELECT WOReasonID FROM dbo.FMS_WORKORDER_REASON fwr WHERE Reason=de.Type and status=1),
							GroupNumb = '00', 
							WorkDesc = de.Subject + char(13) + char(10) + de.description,
							ModifiedBy ='sa', 
							ModifiedDate = GETDATE()
						FROM FMS_WORKORDER_MASTER FWM, FMS_SF_DAILY_EXTRACT DE
						WHERE FWM.WoCode = de.CaseNumber AND FWM.WoCode = @CaseNumber 
						
						PRINT 'Updating WO...1'

						if @Template =''
						BEGIN
							 PRINT 'Updating WO...2'
							UPDATE FMS_WORKORDER_DETAIL 
								SET Comments = de.Subject + char(13) + char(10) + de.description
							FROM FMS_WORKORDER_DETAIL FWD, FMS_SF_DAILY_EXTRACT DE
							WHERE FWD.WoCode = de.CaseNumber AND FWD.WoCode = @CaseNumber 
							
							UPDATE FMS_WO_ACTION_LIST 
								SET ActionList = de.Subject + char(13) + char(10) + de.description
							FROM FMS_WO_ACTION_LIST FWO, FMS_SF_DAILY_EXTRACT DE
							WHERE FWO.WoCode = de.CaseNumber AND FWO.WoCode = @CaseNumber
					    END
					    ELSE  -- in update if Template is not null
					    BEGIN
								PRINT 'Updating WO...3'
								SELECT @LC = LastCounter FROM FMS_SEQUENCE_CONTROL WHERE TableName = 'wo_detail'
								set @LC = @LC+1
								
																
								INSERT INTO FMS_WORKORDER_DETAIL
								(
									WoDetailID,
									WoCode, 
									ActivityCode, 
									Comments,
									CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
								) 
								SELECT @lc + ROW_NUMBER() Over (Order by WoDetailID),
									@CaseNumber, 
									Activitycode,
									Comments,
									'sa' AS CreatedBy, getdate() AS CreatedDate, 'sa' AS ModifiedBy, getdate() AS ModifiedDate
								FROM FMS_WORKORDER_DETAIL FWD 
								WHERE FWD.WoCode=@Template 
								and Activitycode not in (select Activitycode from FMS_WORKORDER_DETAIL where WoCode = @CaseNumber)
								 					
								update FMS_SEQUENCE_CONTROL set LastCounter = (select MAX(WoDetailID) FROM FMS_WORKORDER_DETAIL)
									 where TableName = 'wo_detail' 					
								 					
								 					
								SELECT @LCC = LastCounter FROM FMS_SEQUENCE_CONTROL WHERE TableName = 'WO_ACT_ACTION_LIST'
								set @LCC = @LCC+1
								--update FMS_SEQUENCE_CONTROL set LastCounter = @LCC where TableName = 'WO_ACT_ACTION_LIST'
								PRINT 'Updating WO...4'
								INSERT INTO FMS_WO_ACTION_LIST
								(
									WoActionListID,
									WoCode,
									ActivityCode,
									ActionList,
									CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
								)
								select  @lcc + ROW_NUMBER() Over (Order by WoActionListID),
										@CaseNumber, 
										Activitycode,
										ActionList,
										'sa' AS CreatedBy, getdate() AS CreatedDate, 'sa' AS ModifiedBy, getdate() AS ModifiedDate
								FROM FMS_WO_ACTION_LIST FWO 
								WHERE FWO.WoCode = @Template
								and ActivityCode + '-' + ActionList not in (select ActivityCode + '-' + ActionList from FMS_WO_ACTION_LIST where WoCode=@CaseNumber)
								
								PRINT 'Updating WO...5'
								UPDATE FMS_SEQUENCE_CONTROL set LastCounter = (select MAX(WoActionListID) FROM FMS_WO_ACTION_LIST)
											WHERE TableName = 'WO_ACT_ACTION_LIST'
					    END
						   -- We dont update employee here just insert if the Case ownse does not exists.
						   -- Forget about other employees for that WO
						   
							SELECT @WoEmp = LastCounter FROM FMS_SEQUENCE_CONTROL WHERE TableName = 'wo_emp'
							set @WoEmp = @WoEmp+1
							update FMS_SEQUENCE_CONTROL set LastCounter = @WoEmp where TableName = 'wo_emp'
							
							IF NOT EXISTS (SELECT * FROM FMS_EMPLOYEES FE WHERE FE.FName + ' ' + FE.LName = @SFCaseOwner)
									SET @SFCaseOwner = 'Bill Hollowsky'
							
						   INSERT INTO FMS_WORKORDER_EMPLOYEE
							(
								WoEmpID, WoCode, ActivityCode, EmpTypeID, EmpID, GroupNumb, WorkDate, EndDate
							)
							SELECT TOP 1 @WoEmp, @CaseNumber, @Activity, FE.EmpTypeID, FE.EmpID, FE.GroupNumb,
							@CreatedDate, @CreatedDate
							FROM FMS_EMPLOYEES FE
							WHERE FE.FName + ' ' + FE.LName = @SFCaseOwner and EmpID not in (select EmpID FROM FMS_WORKORDER_EMPLOYEE WHERE WoCode=@CaseNumber)
							ORDER BY EMPID DESC

						PRINT 'WO Employee Completed'
					END -- @Line
						
							--Adding IN House Comments
							SELECT @WoIn = LastCounter FROM FMS_SEQUENCE_CONTROL WHERE TableName = 'WO_INHOUSE_COMMENTS'
							set @WoIn = @WoIn+1
							print @WoIn
							-- first insert any new comments after last sync
							INSERT INTO FMS_WO_INHOUSE_COMMENTS
							(
								WOInHouseID, WoCode, Comments, AddedFrom,
								CreatedBy, CreatedDate, ModifiedBy, ModifiedDate
							)
							SELECT @WoIn + ROW_NUMBER() Over (Order by ID), @CaseNumber, cc.CommentBody, 'W', 
							case when (Select top 1 de.SFCaseOwner from FMS_SF_DAILY_EXTRACT de where de.OwnerID = cc.CreatedByID) like 'Product Dev Team%' then 'Sagar Maramreddy' 
									when (SELECT EmpID FROM FMS_EMPLOYEES FE WHERE FE.FName + ' ' + FE.LName = (Select top 1 de.SFCaseOwner from FMS_SF_DAILY_EXTRACT de where de.OwnerID = cc.CreatedByID)) is null then 'Bill Hollowsky' 
									else (Select top 1 de.SFCaseOwner from FMS_SF_DAILY_EXTRACT de where de.OwnerID = cc.CreatedByID)
								END,
							 
							cc.CreatedDate, 
							case when (Select top 1 de.SFCaseOwner from FMS_SF_DAILY_EXTRACT de where de.OwnerID = cc.LastModifiedByID) like 'Product Dev Team%' then 'Sagar Maramreddy' 
									when (SELECT EmpID FROM FMS_EMPLOYEES FE WHERE FE.FName + ' ' + FE.LName = (Select top 1 de.SFCaseOwner from FMS_SF_DAILY_EXTRACT de where de.OwnerID = cc.LastModifiedByID)) is null then 'Bill Hollowsky' 
									else (Select top 1 de.SFCaseOwner from FMS_SF_DAILY_EXTRACT de where de.OwnerID = cc.LastModifiedByID)
									END,
							
							cc.LastModifiedDate
							FROM FMS_SF_CaseComments cc WHERE ParentId = @ID 
							and cc.CreatedDate not in (select Createddate from FMS_WO_INHOUSE_COMMENTS where WoCode = @CaseNumber)
							--and CreatedDate > (select LastSyncDate from FMS_SYNC where TableName = 'WORKORDER_SF')
							 
							-- Next update the comments if any updates done for the existing ones after last sync.
							update FMS_WO_INHOUSE_COMMENTS set Comments = B.CommentBody
							FROM FMS_WO_INHOUSE_COMMENTS A, FMS_SF_CaseComments B
							WHERE A.WoCode= @CaseNumber and B.ParentId = @ID and A.CreatedDate = B.CreatedDate
							and B.SystemModStamp >= (select LastSyncDate from FMS_SYNC where TableName = 'WORKORDER_SF')
							 
							UPDATE FMS_SEQUENCE_CONTROL set LastCounter = (select MAX(WOInHouseID) FROM FMS_WO_INHOUSE_COMMENTS)
										WHERE TableName = 'WO_INHOUSE_COMMENTS'
							
						INSERT INTO dbo.FMS_WO_SF_Sync_Log
					(
					    ID,
					    CaseNumber,
					    SyncAction,
					    CreatedDate
					)
					VALUES (@ID, @CaseNumber, 'U', GETDATE())	

						PRINT 'Case Table - Update Done'
				END
						NextRecord:
						set @tmpCaseNumber = @CaseNumber
				FETCH NEXT FROM SalesForce INTO @ID, @CaseNumber, @Template, @SFCaseOwner, @CreatedDate, @Activity, @Line
		 	END
		CLOSE SalesForce
		DEALLOCATE SalesForce
				
			-- Updating the Sync date
			SET @ExtractDateTime = (select max(MaxDate) from ((SELECT MaxDate= max(systemmodstamp) FROM FMS_SF_Daily_Extract UNION ALL SELECT MaxDate= max(systemmodstamp) FROM FMS_SF_CaseComments)) X)
			UPDATE FMS_SYNC SET LastSyncDate = isnull(@ExtractDateTime,LastSyncDate)  WHERE TableName = 'WORKORDER_SF'
			PRINT 'Sync table updated for Timestamp'
			
		    SELECT SyncAction, Count(SyncAction) AS SyncCount
		    FROM FMS_WO_SF_Sync_Log
		    GROUP BY SyncAction

END
GO 


 