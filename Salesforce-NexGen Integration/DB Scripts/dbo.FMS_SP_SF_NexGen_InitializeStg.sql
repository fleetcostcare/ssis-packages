USE [NexGen_FCC_Software]
GO

/****** Object:  StoredProcedure [dbo].[FMS_SP_SF_NexGen_InitializeStg]    Script Date: 6/29/2018 3:51:04 PM ******/
DROP PROCEDURE [dbo].[FMS_SP_SF_NexGen_InitializeStg]
GO

/****** Object:  StoredProcedure [dbo].[FMS_SP_SF_NexGen_InitializeStg]    Script Date: 6/29/2018 3:51:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[FMS_SP_SF_NexGen_InitializeStg]
As

Truncate Table [dbo].[FMS_SF_DAILY_EXTRACT]

Truncate Table [dbo].[FMS_SF_JIRA_Extract]

Truncate Table [dbo].[FMS_SF_CaseComments]

Truncate Table [dbo].[FMS_WO_SF_Sync_Log]

Truncate Table [dbo].[FMS_WO_SF_ErrorLog]
GO


