SELECT distinct fsde.NexGenWo, Type AS SFCaseType,Reason as WOReason, fsde.[Priority] as SFPriority, wp.[Description] as WOPriority,fsde.[Status] as SFCaseStatus, wm.WorkStatus as WOWorkStatus
,SFAccount,UnitName as WOAccount, fsde.Department as SFDept, d.Name as WODept
FROM dbo.FMS_SF_DAILY_EXTRACT fsde 
LEFT OUTER JOIN dbo.FMS_WORKORDER_REASON fwr  ON fsde.Type = fwr.reason
LEFT OUTER JOIN dbo.FMS_WORKORDER_PRIORITY wp ON fsde.[Priority] = wp.[Description]
LEFT OUTER JOIN dbo.FMS_WORKORDER_MASTER wm ON wm.WoCode = fsde.NexGenWo
LEFT OUTER JOIN dbo.FMS_UNITS u on WM.UnitCode = u.UnitCode
LEFT OUTER JOIN dbo.FMS_DEPARTMENTS d on wm.DeptCode = d.DeptCode
where UnitName is NULL


select * from FMS_SF_DAILY_EXTRACT

select * from FMS_UNITS
where UnitName like '%Porter%'


update fms_units set UnitName = 'Porter Group' where unitcode = 'PORTER GRP'


update FMS_SYNC
SET LastSyncDate='2018-07-02 13:15:11.000'
where SyncID=12