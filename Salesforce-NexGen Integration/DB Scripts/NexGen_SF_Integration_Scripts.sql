USE [NexGen_FCC_Software]
GO
/****** Object:  Table [dbo].[FMS_SF_CaseComments]    Script Date: 6/22/2018 5:38:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FMS_SF_CaseComments](
	[Id] [nvarchar](18) NULL,
	[ParentId] [nvarchar](18) NULL,
	[IsPublished] [bit] NULL,
	[CommentBody] [nvarchar](4000) NULL,
	[CreatedById] [nvarchar](18) NULL,
	[CreatedDate] [datetime] NULL,
	[SystemModstamp] [datetime] NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [nvarchar](18) NULL,
	[IsDeleted] [bit] NULL,
	[ExtractDateTime] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FMS_SF_DAILY_EXTRACT]    Script Date: 6/22/2018 5:38:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FMS_SF_DAILY_EXTRACT](
	[AccountId] [nvarchar](18) NULL,
	[CaseNumber] [nvarchar](30) NULL,
	[CreatedById] [nvarchar](18) NULL,
	[CreatedDate] [datetime] NULL,
	[Description] [nvarchar](max) NULL,
	[Id] [nvarchar](18) NULL,
	[LastModifiedById] [nvarchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[NexGenJob] [nvarchar](10) NULL,
	[NexGenQuote] [nvarchar](10) NULL,
	[NexGenWO] [nvarchar](10) NULL,
	[OwnerId] [nvarchar](18) NULL,
	[ParentId] [nvarchar](18) NULL,
	[Priority] [nvarchar](255) NULL,
	[Product] [nvarchar](255) NULL,
	[ProductVersion] [nvarchar](10) NULL,
	[RecordTypeId] [nvarchar](18) NULL,
	[RUPApproved] [nvarchar](255) NULL,
	[SFAccount] [nvarchar](1300) NULL,
	[SFCaseOwner] [nvarchar](1300) NULL,
	[Start_Date] [date] NULL,
	[Status] [nvarchar](255) NULL,
	[Subject] [nvarchar](255) NULL,
	[SystemModstamp] [datetime] NULL,
	[Type] [nvarchar](255) NULL,
	[Template] [nvarchar](50) NULL,
	[Activity] [nvarchar](50) NULL,
	[Department] [nvarchar](50) NULL,
	[ExtractDateTime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FMS_SF_JIRA_Extract]    Script Date: 6/22/2018 5:38:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FMS_SF_JIRA_Extract](
	[Id] [nvarchar](18) NULL,
	[OwnerId] [nvarchar](18) NULL,
	[IsDeleted] [bit] NULL,
	[Name] [nvarchar](80) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedById] [nvarchar](18) NULL,
	[LastModifiedDate] [datetime] NULL,
	[LastModifiedById] [nvarchar](18) NULL,
	[SystemModstamp] [datetime] NULL,
	[AffectsVersions] [nvarchar](255) NULL,
	[Assignee] [nvarchar](255) NULL,
	[Components] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Creator] [nvarchar](255) NULL,
	[Deleted_in_JIRA] [bit] NULL,
	[Description] [nvarchar](255) NULL,
	[DueDate] [date] NULL,
	[Environment] [nvarchar](255) NULL,
	[FixVersions] [nvarchar](255) NULL,
	[IssueId] [float] NULL,
	[IssueKey] [nvarchar](30) NULL,
	[IssueType] [nvarchar](255) NULL,
	[OriginalEstimate] [nvarchar](255) NULL,
	[Prioriy] [nvarchar](255) NULL,
	[Project] [nvarchar](255) NULL,
	[RemainingEstimate] [nvarchar](255) NULL,
	[Reporter] [nvarchar](255) NULL,
	[Resolution] [nvarchar](255) NULL,
	[Resolved] [datetime] NULL,
	[Status] [nvarchar](255) NULL,
	[Summary] [nvarchar](255) NULL,
	[TimeSpent] [nvarchar](255) NULL,
	[Updated] [datetime] NULL,
	[Votes] [float] NULL,
	[Watchers] [float] NULL,
	[WorkRatio] [float] NULL,
	[WrongRelation] [bit] NULL,
	[isRelationshipsDirty] [bit] NULL,
	[Epic] [nvarchar](255) NULL,
	[EpicLink] [nvarchar](255) NULL,
	[Sprint] [nvarchar](255) NULL,
	[Story_Points] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FMS_WO_SF_ErrorLog]    Script Date: 6/22/2018 5:38:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FMS_WO_SF_ErrorLog](
	[ID] [nvarchar](18) NULL,
	[NexGenWO] [nvarchar](10) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
	[ErrorDescription] [nvarchar](1024) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FMS_WO_SF_Sync_Log]    Script Date: 6/22/2018 5:38:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FMS_WO_SF_Sync_Log](
	[ID] [nvarchar](18) NOT NULL,
	[CaseNumber] [int] NOT NULL,
	[SyncAction] [nvarchar](10) NOT NULL,
	[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FMS_SF_CaseComments] ADD  CONSTRAINT [DF_FMS_SF_CaseComments_ExtractDateTime]  DEFAULT (getdate()) FOR [ExtractDateTime]
GO
ALTER TABLE [dbo].[FMS_SF_DAILY_EXTRACT] ADD  DEFAULT (getdate()) FOR [ExtractDateTime]
GO
